var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');

//var index = require('./routes/index');
//var users = require('./routes/users');


var dbUrl = "mongodb://dbuser:dbuser@ds227325.mlab.com:27325/mobils"
const db = require('monk')(dbUrl);

const mobils = db.get('mobils');
const users = db.get('users');

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = "123456";

passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
    users.findOne({"_id": jwt_payload._id})
        .then(function (user) {
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        }).catch(function (error) {
            console.log("ERROR " + error);
    });
}));

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

app.post('/api/auth/register', function (req, res) {
    var data = req.body;
    console.log("ASDDF");
    if (!data.username || !data.password) {
        res.json({success: false, msg: 'Si us plau, escriu usuari i contrasenya.'});
    } else {
        users.findOne({username: data.username})
            .then(function (user) {
                console.log(user);

                if (!user) {
                    users.insert(data);
                    return res.json({success: true, msg: 'Usuari creat'});
                } else {
                    return res.json({success: false, msg: 'Usuari ja existeix.'});
                }
            });
    }
});

app.post('/api/auth/login', function (req, res) {
    var data = req.body;

    users.findOne({username: data.username})
        .then(function (user) {
            console.log(user);
            if (!user) {
                res.status(401).send({success: false, msg: 'Autenticació fallida. Usuari no trobat.'});
            } else {
                if (user.password === data.password) {
                    var token = jwt.sign(
                        {
                            "_id": user._id,
                            "username": user.username
                        },
                        opts.secretOrKey
                    );
                    // retorna la informació intruint el token com a json
                    res.json({success: true, token: 'JWT ' + token});
                } else {
                    res.status(401).send({success: false, msg: 'Autenticació fallida. Contrasenya incorrecta.'});
                }
            }
        })
});


app.get('/api/mobils', function(req, res){
    mobils.find({})
        .then(function (data) {
            console.log(data);
            res.json({mobils: data});
        }).catch(function (error) {
        console.log(error);
    });
});

app.post('/api/mobils', function(req, res){
    var data = req.body;

    mobils.insert(data)
        .then(function (result) {
            res.json({"mobils": result});
        }).catch(function (error) {
        console.log(error);
    });
});

app.get('/api/mobils/:imei', function(req, res){
    var _id = req.params.imei;

    mobils.find({"_id": _id})
        .then(function (data) {
            res.json(data);
        }).catch(function (error) {
        console.log(error);
    });
});

app.post('/api/mobils/:imei', function(req, res){
    var _id = req.params.imei;
    var data = req.body;

    mobils.update({"_id": _id}, data)
        .then(function (data) {
            res.json(data);
        }).catch(function (error) {
        console.log(error);
    });
});


app.delete('/api/mobils/:imei', passport.authenticate('jwt', {session: false}), function(req, res){
    if (req.user) {
        console.log(req.user.username)

        var _id = req.params.imei;

        mobils.remove({"_id": _id})
            .then(function (data) {
                console.log("ENTRA");

                res.json(data);
            }).catch(function (error) {
            console.log(error);
        });
    } else {
        return res.status(403).send({success: false, msg: 'Unauthorized'});
    }
});



/*
app.use('/', index);
app.use('/users', users);
*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
